package exceptions;

public class UnauthorizedUser extends Exception {


    public UnauthorizedUser(String message) {
        super(message);
    }

}
