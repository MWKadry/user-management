package com.sumerge.program;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "AUDITLOG", schema = "STRUCTURE")
@NamedQuery(name = "AuditLog.findAll", query = "Select a FROM AuditLog a")

public class AuditLog implements Serializable {

    @Id
    @Column(name = "ID")
    int id;

    @Column(name = "ACTIONNAME")
    String actionName;

    @Column(name = "ACTIONTIME")
    LocalDateTime actionTime;

    @Column(name = "JSONUSER")
    String jsonUser;

    @Column(name = "ACTIONAUTHOR")
    String actionAuthor;

    public AuditLog(String actionName, LocalDateTime actionTime, String jsonUser, String actionAuthor) {
        this.actionName = actionName;
        this.actionTime = actionTime;
        this.jsonUser = jsonUser;
        this.actionAuthor = actionAuthor;
    }


    public AuditLog() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public LocalDateTime getActionTime() {
        return actionTime;
    }

    public void setActionTime(LocalDateTime actionTime) {
        this.actionTime = actionTime;
    }

    public String getJsonUser() {
        return jsonUser;
    }

    public void setJsonUser(String jsonUser) {
        this.jsonUser = jsonUser;
    }

    public String getActionAuthor() {
        return actionAuthor;
    }

    public void setActionAuthor(String actionAuthor) {
        this.actionAuthor = actionAuthor;
    }

    @Override
    public String toString() {
        return "AuditLog{" +
                "id=" + id +
                ", actionName='" + actionName + '\'' +
                ", actionTime=" + actionTime +
                ", jsonUser='" + jsonUser + '\'' +
                ", actionAuthor='" + actionAuthor + '\'' +
                '}';
    }
}
