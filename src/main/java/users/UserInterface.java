package users;

import exceptions.UnauthorizedUser;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


@Path("user")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public interface UserInterface {

    @GET
    @Path("all")
    Response returnAllUsers();

    @GET
    @Path("audit")
    Response returnAuditLog();

    @GET
    @Path("userView")
    Response returnAllUsersUV();

    @GET
    @Path("{userId}/single")
    Response returnUser(@PathParam("userId") Integer userId);

    @GET
    @Path("{userId}/singleUV")
    Response returnUserUV(@PathParam("userId") Integer userId);

    @GET
    @Path("deleted")
    Response returnDeletedUsers();

    @POST
    Response addUser(User user);

    @PUT
    @Path("update")
    Response updateUser(User user);

    @DELETE
    @Path("{userId}/delete")
    Response deleteUser(@PathParam("userId") Integer userId);

    @PUT
    @Path("{userId}/undo")
    Response undoUserDelete(@PathParam("userId") Integer userId);

    @PUT
    @Path("{userId}/{currentGroupId}/{newGroupId}/move")
    Response moveFromTo(@PathParam("userId") Integer userId,
                        @PathParam("currentGroupId") Integer currentGroupId,
                        @PathParam("newGroupId") Integer newGroupId);

    @PUT
    @Path("{userId}/{groupId}/add")
    Response addToGroup(@PathParam("userId") Integer userId,
                        @PathParam("groupId") Integer groupId);

    @PUT
    @Path("{userId}/{groupId}/remove")
    Response removeFromGroup(@PathParam("userId") Integer userId, @PathParam("groupId") Integer groupId) throws UnauthorizedUser;

    @PUT
    @Path("password")
    Response changePassword(UserInfo userInfo) throws UnauthorizedUser;
}
