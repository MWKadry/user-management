package users;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class UserResource implements UserInterface {

    private final Logger LOGGER = Logger.getLogger(users.UserResource.class.getName());

    @EJB
    private UserRepo repo;

    @Context
    SecurityContext securityContext;

    @Override
    public Response returnAuditLog() {
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.getAuditLog()).
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e).
                        build();
            }
        }
        return Response.notModified("Only admin can view see all info").build();
    }

    @Override
    public Response returnAllUsers() {
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.getAllUsers()).
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e).
                        build();
            }
        }
        return Response.notModified("Only admin can view see all info").build();
    }

    @Override
    public Response returnAllUsersUV() {
        try {
            return Response.ok().
                    entity(repo.getAllUsersUV()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @Override
    public Response returnUser(@PathParam("userId") Integer userId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.getUser(userId)).
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e).
                        build();
            }
        }
        return Response.notModified("Only admin can view see all info").build();
    }

    @Override
    public Response returnUserUV(@PathParam("userId") Integer userId) {
        try {
            return Response.ok().
                    entity(repo.getUserUV(userId)).
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @Override
    public Response returnDeletedUsers() {
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.getAllDeletedUsers()).
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e).
                        build();
            }
        }
        return Response.notModified("Only admin can view deleted users").build();
    }

    @Override
    public Response addUser(User user) {
        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.addUser(user, author);
                return Response.ok().
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e.getMessage()).
                        build();
            }
        }
        return Response.notModified("Only admin can add user").build();
    }

    @Override
    public Response updateUser(User user) {
        if ((securityContext.isUserInRole("admin") ||
                (securityContext.getUserPrincipal().getName() == user.getUserName()))
                && user.getUserId() != 1) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.updateUser(user, author);
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
        }
        return Response.notModified("User not authorized").build();
    }

    @Override
    public Response deleteUser(@PathParam("userId") Integer userId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.userSoftDelete(userId, author);
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        return Response.notModified("Only admin can delete user").build();
    }

    @Override
    public Response undoUserDelete(@PathParam("userId") Integer userId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.undoUserDelete(userId, author);
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
        }
        return Response.notModified("Only admin can undo delete").build();
    }

    @Override
    public Response moveFromTo(@PathParam("userId") Integer userId,
                               @PathParam("currentGroupId") Integer currentGroupId,
                               @PathParam("newGroupId") Integer newGroupId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.moveUserToGroup(userId, currentGroupId, newGroupId, author);
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
        }
        return Response.notModified("Only admin can move user to new group").build();
    }

    @Override
    public Response addToGroup(@PathParam("userId") Integer userId, @PathParam("groupId") Integer groupId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.addUserToGroup(userId, groupId, author);
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
        }
        return Response.notModified("Only admin can add user to new group").build();
    }

    @Override
    public Response removeFromGroup(@PathParam("userId") Integer userId, @PathParam("groupId") Integer groupId)  {
        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.removeUserFromGroup(userId, groupId, author);
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
        }
        return Response.notModified("Only admin can remove user to new group").build();
    }

    @Override
    public Response changePassword(UserInfo userInfo){

        String name = securityContext.getUserPrincipal().getName();
        if ((name != userInfo.getUserName()) || (userInfo.getUserId() == 1)) {
            try {
                repo.changeMyPassword(userInfo, name);
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        return Response.notModified("Only admin can remove user to new group").build();
    }

}
