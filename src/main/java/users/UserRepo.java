package users;

import com.google.gson.Gson;
import com.sumerge.program.AuditLog;
import groups.Group;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class UserRepo {

    private static final Logger LOGGER = Logger.getLogger(users.UserRepo.class.getName());

    @PersistenceContext(unitName = "test")
    private EntityManager em;


    public List<AuditLog> getAuditLog() {
        LOGGER.info("Fetching Audit Log list");
        try {
            List<AuditLog> auditLogs = em.createQuery("Select a from AuditLog a", AuditLog.class).getResultList();
            return auditLogs;

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public List<User> getAllUsers() {
        LOGGER.info("Fetching users list");
        try {
            List<User> users = em.createNamedQuery("User.findAll", User.class).getResultList();
            return users;

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public List<UserView> getAllUsersUV() {
        LOGGER.info("Fetching users list");
        try {
            List<User> users = em.createNamedQuery("User.findAll", User.class).getResultList();
            List<UserView> userViews = new ArrayList<>();

            for (User user : users) {
                userViews.add(new UserView(user.getPhoneNumber(), user.getAddress(), user.getUserName(),
                        user.getEmail(), user.getUserRole(), user.getFamilyName()));
            }
            return userViews;

        } catch (Exception e) {
            throw e;
        }
    }

    public List<User> getAllDeletedUsers() {
        LOGGER.info("Fetching deleted users list");
        try {
            return em.createNamedQuery("User.findDeleted", User.class).getResultList();

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public UserView getUserUV(Integer userId) {
        LOGGER.info("Fetching user");
        try {
            User user = em.find(User.class, userId);

            if (user.getUserDeleted() == 1 || user == null)
                throw new IllegalArgumentException("User with ID: " + userId + "does not exist in the database");

            UserView userView = new UserView(user.getPhoneNumber(), user.getAddress(), user.getUserName(),
                    user.getEmail(), user.getUserRole(), user.getFamilyName());

            return userView;

        } catch (Exception e) {
            throw e;
        }
    }

    public User getUser(Integer userId) {
        LOGGER.info("Fetching user" + userId);
        try {
            User user = em.find(User.class, userId);

            if (user.getUserDeleted() == 1 || user == null)
                throw new IllegalArgumentException("User with ID: " + userId + "does not exist in the database");
            return user;

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addUser(User user, String author) throws Exception {
        LOGGER.info("Adding new user " + user);
        try {
            String json = new Gson().toJson(user.toString());
            AuditLog audit = new AuditLog("Add User", LocalDateTime.now(), json, author);
            user.setPassword(sha256(user.getPassword()));
            em.persist(user);
            em.persist(audit);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateUser(User user, String author) {
        LOGGER.info("Updating user " + user);
        try {


            if (user.getUserDeleted() == 1 || user == null)
                throw new IllegalArgumentException("User " + user + "does not exist in the database");
            if (user.getUserRole() == "admin")
                throw new IllegalArgumentException("Admin can not be updated");

            String json = new Gson().toJson(user.toString());
            AuditLog audit = new AuditLog("Update User", LocalDateTime.now(), json, author);
            em.merge(user);
            em.persist(audit);


        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }

    }

    public void userSoftDelete(Integer userId, String author) {
        LOGGER.info("Deleting  user " + userId);
        try {

            User user = em.find(User.class, userId);

            if (user.getUserDeleted() == 1 || user == null)
                throw new IllegalArgumentException("User with ID: " + userId + "does not exist in the database");
            if (user.getUserRole() == "admin")
                throw new IllegalArgumentException("Admin can not be deleted");

            String json = new Gson().toJson(user.toString());
            AuditLog audit = new AuditLog("Delete User", LocalDateTime.now(), json, author);

            user.setUserDeleted(1);
            em.merge(user);
            em.persist(audit);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void undoUserDelete(Integer userId, String author) {
        LOGGER.info("Retrieving  user " + userId);
        try {

            User user = em.find(User.class, userId);

            if (user.getUserDeleted() == 0)
                throw new IllegalArgumentException("User with ID: " + userId + "already exists in the database");

            String json = new Gson().toJson(user.toString());
            AuditLog audit = new AuditLog("Undo Delete", LocalDateTime.now(), json, author);

            user.setUserDeleted(0);
            em.merge(user);
            em.persist(audit);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void moveUserToGroup(Integer userId, Integer currentGroupId, Integer newGroupId, String author) {
        LOGGER.info("moving user " + userId + "to group" + newGroupId);
        try {

            User user = em.find(User.class, userId);
            Group newGroup = em.find(Group.class, newGroupId);
            Group currentGroup = em.find(Group.class, currentGroupId);

            if (user.getUserRole() != "admin" && newGroupId == 1)
                throw new IllegalArgumentException("User with ID: " + userId + "is not admin");
            if (user.getUserDeleted() == 1 || user == null)
                throw new IllegalArgumentException("User with ID: " + userId + "does not exist in the database");
            if (newGroup.getGroupDeleted() == 1 || newGroup == null)
                throw new IllegalArgumentException("Group with ID: " + newGroupId + "does not exist in the database");
            if (currentGroup.getGroupDeleted() == 1 || currentGroup == null)
                throw new IllegalArgumentException("Group with ID: " + currentGroupId + "does not exist in the database");

            user.getGroups().add(newGroup);
            newGroup.getUsers().add(user);
            user.getGroups().remove(currentGroup);
            currentGroup.getUsers().remove(user);

            String json = new Gson().toJson(user.toString());
            AuditLog audit = new AuditLog("Moving User to new group", LocalDateTime.now(), json, author);

            em.merge(user);
            em.merge(newGroup);
            em.merge(currentGroup);
            em.persist(audit);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addUserToGroup(Integer userId, Integer groupId, String author) {
        LOGGER.info("moving user " + userId + " to group " + groupId);
        try {

            User user = em.find(User.class, userId);
            Group group = em.find(Group.class, groupId);

            if (user.getUserRole() != "admin" && groupId == 1)
                throw new IllegalArgumentException("User with ID: " + userId + "is not admin");
            if (user.getUserDeleted() == 1 || user == null)
                throw new IllegalArgumentException("User with ID: " + userId + "does not exist in the database");
            if (group.getGroupDeleted() == 1 || group == null)
                throw new IllegalArgumentException("Group with ID: " + groupId + "does not exist in the database");

            user.getGroups().add(group);
            group.getUsers().add(user);

            String json = new Gson().toJson(user.toString());
            AuditLog audit = new AuditLog("Adding User to group", LocalDateTime.now(), json, author);

            em.merge(user);
            em.merge(group);
            em.persist(audit);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }

    }

    public void removeUserFromGroup(Integer userId, Integer groupId, String author) {
        LOGGER.info("moving user " + userId + "to group" + groupId);
        try {

            User user = em.find(User.class, userId);
            Group group = em.find(Group.class, groupId);

            if (user.getUserRole() == "admin" && groupId == 1)
                throw new IllegalArgumentException("Admin can not be removed");
            if (user.getUserDeleted() == 1 || user == null)
                throw new IllegalArgumentException("User with ID: " + userId + "does not exist in the database");
            if (group.getGroupDeleted() == 1 || group == null)
                throw new IllegalArgumentException("Group with ID: " + groupId + "does not exist in the database");

            user.getGroups().remove(group);
            group.getUsers().remove(user);

            String json = new Gson().toJson(user.toString());
            AuditLog audit = new AuditLog("Removing User from group", LocalDateTime.now(), json, author);

            em.merge(user);
            em.merge(group);
            em.persist(audit);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void changeMyPassword(UserInfo userInfo, String name) throws Exception {
        LOGGER.info("Changing password");
        try {

            String oldPassword = sha256(userInfo.getOldPassword());
            String newPassword = sha256(userInfo.getNewPassword());

            String json = new Gson().toJson(userInfo.toString());
            AuditLog audit = new AuditLog("Changing password", LocalDateTime.now(), json, name);

            User user = em.createNamedQuery("User.findByName", User.class).setParameter("userName", name).getSingleResult();

            LOGGER.info(oldPassword);
            LOGGER.info(user.getPassword());

            if (oldPassword.equals(user.getPassword())) {
                user.setPassword(newPassword);
                em.merge(user);
                em.persist(audit);
            } else {
                throw new IllegalArgumentException("Password is wrong");
            }

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public static String sha256(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md5 = MessageDigest.getInstance("SHA-256");
        byte[] digest = md5.digest(input.getBytes("UTF-8"));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; ++i) {
            sb.append(Integer.toHexString((digest[i] & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }

}
