package users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import groups.Group;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "USERS", schema = "STRUCTURE")

@NamedQueries({
        @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u Where u.userDeleted = 0"),
        @NamedQuery(name = "User.findDeleted", query = "SELECT u FROM User u Where u.userDeleted = 1"),
        @NamedQuery(name = "User.findByName", query = "SELECT u From User u Where u.userName = :userName")
})

public class User implements Serializable {
    @Id
    @Column(name = "USERID")
    private Integer userId;

    @Column(name = "PHONENUMBER")
    private Integer phoneNumber;

    @Column(name = "USERDELETED")
    private Integer userDeleted = 0;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ROLE")
    private String userRole;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "FAMILYNAME")
    private String familyName;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "GROUPMEMBERS",
            joinColumns = @JoinColumn(name = "USERID"),
            inverseJoinColumns = @JoinColumn(name = "GROUPID"))
    private List<Group> groups = new ArrayList<>();

    public User() {
    }

    public User( Integer phoneNumber, Integer userDeleted, String address, String userName, String email, String userRole, String password, String familyName) {
        this.phoneNumber = phoneNumber;
        this.userDeleted = userDeleted;
        this.address = address;
        this.userName = userName;
        this.email = email;
        this.userRole = userRole;
        this.password = password;
        this.familyName = familyName;
        this.groups = new ArrayList<>();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Integer getUserDeleted() {
        return userDeleted;
    }

    public void setUserDeleted(Integer userDeleted) {
        this.userDeleted = userDeleted;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

}
