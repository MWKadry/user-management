package users;

public class UserView {

    private Integer phoneNumber;
    private String address;
    private String userName;
    private String email;
    private String userRole;
    private String familyName;

    public UserView() {
    }

    public UserView(Integer phoneNumber, String address, String userName,
                    String email, String userRole, String familyName) {
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.userName = userName;
        this.email = email;
        this.userRole = userRole;
        this.familyName = familyName;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) { this.address = address; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }
}
