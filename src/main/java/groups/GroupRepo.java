package groups;

import com.google.gson.Gson;
import com.sumerge.program.AuditLog;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class GroupRepo {

    private static final Logger LOGGER = Logger.getLogger(groups.GroupRepo.class.getName());


    @PersistenceContext(unitName = "test")
    private EntityManager em;

    public List<Group> getAllGroups() {
        LOGGER.info("Fetching groups list");
        try {
            return em.createNamedQuery("Group.findAll", Group.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public List<Group> getAllDeletedGroups() {
        LOGGER.info("Fetching deleted groups list");
        try {
            return em.createNamedQuery("Group.findDeleted", Group.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public Group getGroup(Integer groupId) {
        LOGGER.info("Fetching group");
        try {
            Group group = em.find(Group.class, groupId);
            if (group.getGroupDeleted() == 1 || group == null)
                throw new IllegalArgumentException("Group with ID: " + groupId + "does not exist in the database");
            return group;
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addGroup(Group group, String author) {
        LOGGER.info("Adding new group " + group);
        try {
            String json = new Gson().toJson(group.toString());
            AuditLog audit = new AuditLog("Adding group", LocalDateTime.now(), json, author);
            em.persist(group);
            em.persist(audit);

        } catch (Exception e) {
            try {

            } catch (Exception u) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                throw e;
            }
        }
    }

    public void updateGroup(Group group, String author) {
        LOGGER.info("Updating group " + group);
        try {


            if (group.getGroupDeleted() == 1 || group == null)
                throw new IllegalArgumentException("Group " + group + "does not exist in the database");

            String json = new Gson().toJson(group.toString());
            AuditLog audit = new AuditLog("Updating group", LocalDateTime.now(), json, author);

            em.merge(group);
            em.persist(audit);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }

    }

    public void groupSoftDelete(Integer groupId, String author) {
        LOGGER.info("Deleting  group " + groupId);
        try {

            Group group = em.find(Group.class, groupId);

            if (group.getGroupDeleted() == 1 || group == null)
                throw new IllegalArgumentException("Group with ID: " + groupId + "does not exist in the database");
            if (group.getGroupId() == 1)
                throw new IllegalArgumentException("Admin group can not be deleted");

            group.setGroupDeleted(1);

            String json = new Gson().toJson(group.toString());
            AuditLog audit = new AuditLog("Delete Group", LocalDateTime.now(), json, author);
            em.merge(group);
            em.persist(audit);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void undoGroupDelete(Integer groupId, String author) {
        LOGGER.info("Retrieving  group " + groupId);
        try {

            Group group = em.find(Group.class, groupId);

            if (group.getGroupDeleted() == 0)
                throw new IllegalArgumentException("Group with ID: " + groupId + "already exists in the database");

            group.setGroupDeleted(0);

            String json = new Gson().toJson(group.toString());
            AuditLog audit = new AuditLog("Undo group delete", LocalDateTime.now(), json, author);
            em.merge(group);
            em.persist(audit);

        } catch (Exception e) {
            try {

            } catch (Exception u) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                throw e;
            }
        }
    }


}
