package groups;

import users.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "GRPS", schema = "STRUCTURE")
@NamedQueries({
        @NamedQuery(name = "Group.findAll", query = "SELECT g FROM Group g Where g.groupDeleted = 0"),
        @NamedQuery(name = "Group.findDeleted", query = "SELECT g FROM Group g Where g.groupDeleted = 1")
})
public class Group implements Serializable {

    @Id
    @Column(name = "GROUPID")
    private Integer groupId;

    @Column(name = "GROUPDELETED")
    private Integer groupDeleted = 0;

    @Column(name = "GROUPNAME")
    private String groupName;

    @ManyToMany
    @JoinTable(
            name = "GROUPMEMBERS",
            joinColumns = @JoinColumn(name = "GROUPID"),
            inverseJoinColumns = @JoinColumn(name = "USERID"))
    private List<User> users;

    public Group() {
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getGroupDeleted() {
        return groupDeleted;
    }

    public void setGroupDeleted(Integer groupDeleted) {
        this.groupDeleted = groupDeleted;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
