package groups;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class GroupResource implements GroupInterface {

    private static final Logger LOGGER = Logger.getLogger(groups.GroupResource.class.getName());

    @EJB
    private GroupRepo repo;

    @Context
    SecurityContext securityContext;

    @Override
    public Response returnAllGroups() {
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.getAllGroups()).
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e).
                        build();
            }
        }
        return Response.notModified("Only admin can view all groups").build();
    }

    @Override
    public Response returnGroup(@PathParam("groupId") Integer groupId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.getGroup(groupId)).
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e).
                        build();
            }
        }
        return Response.notModified("Only admin can view group").build();
    }

    @Override
    public Response returnDeletedGroups() {
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.getAllDeletedGroups()).
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e).
                        build();
            }
        }
        return Response.notModified("Only admin can view deleted groups").build();
    }

    @Override
    public Response addGroup(Group group) {

        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.addGroup(group, author);
                return Response.ok().
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e.getMessage()).
                        build();
            }
        }
        return Response.notModified("Only admin can add group").build();
    }

    @Override
    public Response updateGroup(Group group) {
        if ((securityContext.isUserInRole("admin") ||
                (securityContext.getUserPrincipal().getName() == group.getGroupName()))
                && group.getGroupId() != 1) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.updateGroup(group, author);
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
        }
        return Response.notModified("User not authorized").build();
    }

    @Override
    public Response deleteGroup(@PathParam("groupId") Integer groupId) {

        System.out.println(groupId + "");
        System.out.println(securityContext.getUserPrincipal().toString());

        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.groupSoftDelete(groupId, author);
                return Response.ok().
                        build();
            } catch (IllegalArgumentException e) {
                return Response.status(400, e.getMessage()).
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        return Response.notModified("Only admin can delete group").build();
    }

    @Override
    public Response undoGroupDelete(@PathParam("groupId") Integer groupId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                String author = securityContext.getUserPrincipal().toString();
                repo.undoGroupDelete(groupId, author);
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
        }
        return Response.notModified("Only admin can undo delete").build();
    }
}

