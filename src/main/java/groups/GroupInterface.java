package groups;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("group")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public interface GroupInterface {

    @GET
    @Path("all")
    Response returnAllGroups();

    @GET
    @Path("{groupId}/single")
    Response returnGroup(@PathParam("groupId") Integer groupId);

    @GET
    @Path("deleted")
    Response returnDeletedGroups();

    @POST
    Response addGroup(Group group);

    @PUT
    Response updateGroup(Group group);

    @DELETE
    @Path("{groupId}/delete")
    Response deleteGroup(@PathParam("groupId") Integer groupId);

    @PUT
    @Path("{groupId}/undo")
    Response undoGroupDelete(@PathParam("groupId") Integer groupId);

}
