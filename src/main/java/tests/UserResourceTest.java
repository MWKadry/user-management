package tests;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.common.util.Base64Utility;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import users.User;
import users.UserInterface;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserResourceTest {

    private static UserInterface client;

    @BeforeClass
    public static void init() {
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JacksonJaxbJsonProvider());
        client = JAXRSClientFactory.create("http://localhost:8880/",
                UserInterface.class, providers);

    }

    @org.junit.Test
    public void testAddNewUSer() {

        String auth = "Basic " + Base64Utility.encode(("admin:111").getBytes());
        WebClient.client(client).reset();
        WebClient.client(client).header("Authorization", auth);

        Response response = client.addUser(new User(12345667, 0, "Madinaty", "user14",
                "sfkjsndf", "user", "3354", "aa"));
        assertTrue("Error", response.getStatus() == 200);


    }
}
